#include "HeatMap.hpp"

#include "flip_utils.hpp"

using poss_t = byte;
const poss_t NO_ONE = 0;
const poss_t US = 1;
const poss_t THEM = 2;

using robot_pos_t = std::tuple<Position, std::optional<Position>, std::optional<Position>, std::optional<Position>>;

static const bool is_pos_occupied_by(uint8_t i, uint8_t j, Position& robot_pos) {
    auto &[x, y] = pos_to_square(robot_pos);
    return x == i && y == j;
} 

static const bool is_pos_occupied_by_opt(uint8_t i, uint8_t j, std::optional<Position>& opt_robot_pos) {
    if (!opt_robot_pos)
        return false;
    auto robot_pos = *opt_robot_pos;

    return is_pos_occupied_by(i, j, robot_pos);
} 

TileContent get_tile_content(MazeSquare* sq, uint8_t i, uint8_t j, robot_pos_t& positions) {
    // if (still_in_maze()) {
    //     return TileContent::NOT_AVAILABLE;
    // }
    
    auto &[me, opt_bro, opt_opp1, opt_opp2] = positions;

    if (is_pos_occupied_by(i, j, me)) {
        return TileContent::ME;
    }

    if (is_pos_occupied_by_opt(i, j, opt_bro) 
        || is_pos_occupied_by_opt(i, j, opt_opp1) 
        || is_pos_occupied_by_opt(i, j, opt_opp2)) {
        return TileContent::ROBOT;
    }

    if (sq->isBomb) {
        return TileContent::DROPPED_BOMB;
    }

    // unused atm
    switch (sq->danger)
    {
    default:
        break;
    } 
    
    if (sq->coin.value >= 0) {
        return TileContent::LOOTABLE_BOMB;
    }

    switch (sq->possession)
    {
        case US : 
            return TileContent::MY_COLOR;
        case THEM : 
            return TileContent::THEIR_COLOR;
        default:
            return TileContent::NOTHING;
    }
}


score_grid_t score_maze(Maze* maze, robot_pos_t& robots_pos)
{
    
    std::array<TileContent, MAX_MAZE_SIZE> maze_mapping;

    // INITIAL MAPPING
    for (int i = 0; i < MAX_MAZE ; ++i) {
        for (int j = 0; j < MAX_MAZE ; ++j) {
            auto curr_square = maze->getSquare(i, j);
            TileContent curr_state = get_tile_content(curr_square, i, j, robots_pos);
            maze_mapping[i*MAX_MAZE + j] = curr_state;
        }
    }

    score_grid_t current_maze_score;

    for (int i = 0; i < MAX_MAZE ; ++i) {
        for (int j = 0; j < MAX_MAZE ; ++j) {
            apply_kernel(maze_mapping, &current_maze_score, i, j);
        }
    }
    return current_maze_score;
}

robot_pos_t get_robots_pos(Gladiator& me) {
     // Obtenir les données du robot
    RobotData my_data = me.robot->getData(); 

    // on récupère la liste des robots actuellement dans la partie
    auto game = me.game;
    auto robot_list = game->getPlayingRobotsId(); // GFA 4.3.5
    std::optional<Position> bro_pos {};
    std::optional<Position> opp1_pos{};
    std::optional<Position> opp2_pos{};

    for (uint8_t id : robot_list.ids)
    {

        if (id != 0 && id != my_data.id)
        {

            auto o = game->getOtherRobotData(id); // 4.3.3
            if (o.teamId == my_data.teamId) 
                bro_pos = o.position;
            else if (!opp1_pos) 
                opp1_pos = o.position;
            else 
                opp2_pos = o.position;
        }
    }
    
    return std::tuple{my_data.position, bro_pos, opp1_pos, opp2_pos};
}

score_grid_t heatmap_create(Gladiator& me) {
    auto pos = get_robots_pos(me);
    auto maze = me.maze;

    auto score = score_maze(maze, pos);

    return score; 
}

void apply_bomb_ratio(score_grid_t *coeffs, uint8_t i, uint8_t j, uint8_t number_of_bombs_stacked)
{
    for(int idx = 1; idx <= number_of_bombs_stacked; ++idx) {
        auto prev_i = i - idx;
        auto prev_j = j - idx;
        auto next_i = i + idx;
        auto next_j = j + idx;

        if (!is_out(prev_i)) {
            auto& curr = coeffs->at(prev_i * MAX_MAZE + j);
            curr -= DROPPED_BOMB;
        }
        if (!is_out(prev_j)) {
            auto& curr = coeffs->at(i * MAX_MAZE + prev_j);
            curr -= DROPPED_BOMB;
        }
        if (!is_out(next_i)) {
            auto& curr = coeffs->at(next_i * MAX_MAZE + j);
            curr -= DROPPED_BOMB;
        }
        if (!is_out(next_j)) {
            auto& curr = coeffs->at(i * MAX_MAZE + next_j);
            curr -= DROPPED_BOMB;
        }
    }
}

void apply_kernel_radius(score_grid_t *coeffs, uint8_t i, uint8_t j, uint8_t radius, int8_t score)
{
    for (int x = -radius ; x <= radius ; ++x) {
        auto col = i+x;
        if (is_out(col)) {
            continue;
        }
        for (int y = -radius ; y <= radius ; ++y) {
            auto row = j+y;
            if (is_out(row)) {
                continue;
            }
            auto& curr = coeffs->at(col * MAX_MAZE + row);
            auto two_rad = radius * 2;
            auto rad_mult = two_rad - abs(x) - abs(y);
            auto new_score = rad_mult * score / two_rad;
            curr += new_score;
        }
    }
}

void apply_kernel(std::array<TileContent, MAX_MAZE_SIZE>& maze_mapping, score_grid_t *coeffs, uint8_t i, uint8_t j)
{
    auto tile = maze_mapping.at(i * MAX_MAZE + j);

    uint8_t radius = 0;
    int8_t score  = INT8_MIN;

    switch (tile)
    {
    case TileContent::DROPPED_BOMB:
        apply_bomb_ratio(coeffs, i, j, 1);
        return;     
    case TileContent::NOTHING:
    case TileContent::MY_COLOR:
    case TileContent::THEIR_COLOR:
        score = 1;
        break;
    case TileContent::LOOTABLE_BOMB:
        score = 50;
        radius = 1;
        break;
    case TileContent::ROBOT:
        score = -100;
        radius = 2;
        break;
    default:
        break;
    }

    apply_kernel_radius(coeffs, i, j, radius, score);
}
