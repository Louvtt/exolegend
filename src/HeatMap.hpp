#ifndef EXOL_V2_HEATMAP_H
#define EXOL_V2_HEATMAP_H

#include "stdint.h"
#include "gladiator.h"
#include <array>
#include <optional>
#include <tuple>
#include <cassert>
#include "flip_utils.hpp"


using score_grid_t = std::array<int8_t, MAX_MAZE_SIZE>;
[[nodiscard]] score_grid_t heatmap_create(Gladiator& me);

score_grid_t score_maze(Maze &maze);

enum class TileContent : int  {
    NOT_AVAILABLE = 0, // rad 1           | never (neg or null)
    NOTHING       = 1, // rad 1           | 1
    MY_COLOR      = 2, // rad 1           | 1
    THEIR_COLOR   = 3, // rad 1           | pos
    DROPPED_BOMB  = 4, // 1 + bomb number | neg
    LOOTABLE_BOMB = 5, // rad 2           | pos
    ROBOT         = 6, // rad 3           | neg
    ME            = 7  // rad 1           | never (neg or null)
};

void apply_kernel(std::array<TileContent, MAX_MAZE_SIZE>& maze_mapping, score_grid_t *coeffs, uint8_t i, uint8_t j);
void apply_bomb_ratio(score_grid_t* coeffs, uint8_t i, uint8_t j, uint8_t number_of_bombs_stacked);
void apply_kernel_radius(score_grid_t* coeffs, uint8_t i, uint8_t j, uint8_t radius, int8_t score);

// single tiles
const int8_t NOT_AVAILABLE         = INT8_MIN;
const int8_t DROPPED_BOMB          = 50;
const int8_t NOTHING_SPECIAL       = 1;
const int8_t ALREADY_COLORED_TILE  = 1; // our color
const int8_t EMPTY_TILE            = INT8_MAX / 2;
const int8_t COLORED_TILE          = INT8_MAX / 2; // ennemy color
const int8_t LOOTABLE_BOMB         = INT8_MAX;
const int8_t OTHER_ROBOT           = INT8_MIN;
const int8_t FOURTH                = 128 / 4; // 32


#endif //EXOL_V2_HEATMAP_H
