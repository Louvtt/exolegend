#include "Strategy.hpp"

#include "data.h"
#include "vector2.hpp"

double reductionAngle(double x)
{
    x = fmod(x + PI, 2 * PI);
    if (x < 0)
        x += 2 * PI;
    return x - PI;
}

bool Strategy::go_to(Position target, Position current)
{
    double consvl, consvr;
    const double dx = target.x - current.x;
    const double dy = target.y - current.y;
    const double d = sqrt(dx * dx + dy * dy);
    bool reached = false;

    if (d > erreurPos)
    {
        double rho = atan2(dy, dx);
        double angleErr = reductionAngle(rho - current.a);
        double consw = kw * angleErr;

        double consv = kv * d * cos(angleErr);
        consw = abs(consw) > wlimit ? (consw > 0 ? 1 : -1) * wlimit : consw;
        consv = abs(consv) > vlimit ? (consv > 0 ? 1 : -1) * vlimit : consv;

        if (std::abs(angleErr) > angleThreashold) {
          consvl = - gladiator->robot->getRobotRadius() * consw; // GFA 3.6.2
          consvr = + gladiator->robot->getRobotRadius() * consw; // GFA 3.6.2
        } else {
          consvl = consv - gladiator->robot->getRobotRadius() * consw; // GFA 3.6.2
          consvr = consv + gladiator->robot->getRobotRadius() * consw; // GFA 3.6.2
        }
    }
    else
    {
        consvr = 0;
        consvl = 0;
        reached = true;
    }

    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, consvr, false); // GFA 3.2.1
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, consvl, false);  // GFA 3.2.1
    return reached;
}

bool Strategy::go_to_square(MazeSquare *square) {
    if(!square) return true; // failsafe
    const Position current = gladiator->robot->getData().position;
    const Position next    = (Position){ ((float)square->i + .5f) * square_size, ((float)square->j + .5f) * square_size };
    return go_to(next, current);
}

MazeSquare* Strategy::find_next() {
  const auto current = gladiator->maze->getNearestSquare();
  if (current->eastSquare != nullptr) return current->eastSquare;
  if (current->westSquare != nullptr) return current->westSquare;
  if (current->northSquare != nullptr) return current->northSquare;
  if (current->southSquare != nullptr) return current->southSquare;
  return nullptr;
}

void Strategy::setup() {
  square_size = gladiator->maze->getSquareSize();
  start_millis = millis();
}


void Strategy::reset() {
  start_millis = millis();
}

bool Strategy::is_accessible(MazeSquare *sq) const {
  const size_t turn = get_turn(200); // assume we take 200ms to go to the cell
  if (sq->i < turn || 11-turn < sq->i) return false;
  if (sq->j < turn || 11-turn < sq->j) return false;
  return true;
}

size_t Strategy::get_turn(size_t ms_offset) const {
  return (millis() - start_millis + ms_offset) / 20000;
}

void Strategy::start() {
  next_target = find_next();
  start_millis = millis();
}

void Strategy::update() {
    if (next_target != nullptr) {
        if(go_to_square(next_target)) {
            next_target = find_next(); // tell to search next square
        }
    } else {
      next_target = find_next();
    }
}

