#ifndef EXOLEGEND_STRATEGY_H
#define EXOLEGEND_STRATEGY_H

#include "gladiator.h"
#include <array>

#define DEFAULT_MAP_SIZE 12
#define NUMBER_OF_CASES DEFAULT_MAP_SIZE * DEFAULT_MAP_SIZE

/* Group strategy */
struct Strategy {
    Gladiator* gladiator {nullptr};
    MazeSquare* next_target {nullptr};

    /* Assertion params */
    float square_size = 0.1f;
    int map_size = DEFAULT_MAP_SIZE;
    std::array<std::array<MazeSquare *, DEFAULT_MAP_SIZE>, DEFAULT_MAP_SIZE> map = {nullptr};

    /* Strategy dependent methods */
    virtual void setup();

    /// when the game starts
    virtual void start();
    /// When we re use the robot
    virtual void reset();
    /// For each game update
    virtual void update();
   /// Returns the next square to go to
    virtual MazeSquare *find_next();

    size_t get_turn(size_t ms_offset = 0) const;
    bool is_accessible(MazeSquare *sq) const;

    /* Trajectory assertion */

    bool go_to(Position target, Position current);
    bool go_to_square(MazeSquare* square);

private:
  size_t start_millis;
};


#endif //EXOLEGEND_STRATEGY_H
