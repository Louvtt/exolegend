#include "Task.hpp"
#include "bomb_task.hpp"

TaskScheduler * g_task_scheduler = new TaskScheduler();

void TaskScheduler::setup(Gladiator* gladiator) {
  this->gladiator = gladiator;
}

void TaskScheduler::update() {
  // check for interrupt tasks conditions
  if (gladiator->weapon->getBombCount() > 0 && !not_planned_to_plant) {
    add_task_interrupt(new LayDownBombTask());
    not_planned_to_plant = true;
  }

  if(tasks.empty()) {
    // default task
    add_task(new GetNearestBombTask());
  } else {
    auto task = tasks.front();
    task->update(gladiator);
    if (task->is_done()) {
      tasks.pop_front();
    }
  }
}

void TaskScheduler::reset() {
  tasks.clear();
  start_time = millis();
  not_planned_to_plant = false;
}

void TaskScheduler::start() {
  start_time = millis();
}

size_t TaskScheduler::get_turn_count(size_t ms_offset) const {
  return (millis() - start_time + ms_offset) / 20000;
}

bool TaskScheduler::is_accessible(MazeSquare *sq) const {
  if (sq == nullptr) return false;

  const size_t turn = get_turn_count(1000); // assume we take 200ms to go to the cell
  if (sq->i < turn || 11-turn < sq->i) return false;
  if (sq->j < turn || 11-turn < sq->j) return false;
  return true;
}

bool TaskScheduler::is_safe(MazeSquare *sq) const {
  if (sq == nullptr) return false;
  if(!is_accessible(sq)) return false;

  // level 1 bomb
  if(sq->isBomb)                                 return false;
  if(sq->northSquare && sq->northSquare->isBomb) return false;
  if(sq->eastSquare  && sq->eastSquare->isBomb)  return false;
  if(sq->southSquare && sq->southSquare->isBomb) return false;
  if(sq->westSquare  && sq->westSquare->isBomb)  return false;
  return true;
}


void TaskScheduler::add_task(Task *task)
{
  gladiator->log("Added task %p", task);
  tasks.push_back(task);
}

void TaskScheduler::add_task_interrupt(Task *task) {
  if (tasks.empty()) {
    tasks.push_front(task);
    return;
  }

  auto current = tasks.front();
  if (task->interruptable) {
    // push as the first task
    tasks.push_front(task);
  } else {
    // push after the current task
    // so as the second task
    tasks.pop_front();
    tasks.push_front(task);
    tasks.push_front(current);
  }
}
