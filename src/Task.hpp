#ifndef EXOLEGEND_TASK_HPP
#define EXOLEGEND_TASK_HPP

#include "gladiator.h"

#include <deque>

struct Task {
  uint32_t id;
  bool interruptable;

  virtual bool is_done() const           = 0;
  virtual void update(Gladiator *gladiator) = 0;
};

struct TaskScheduler {
  void setup(Gladiator* gladiator);
  void update();
  void reset();
  void start();

  void add_task_interrupt(Task* task);
  void add_task(Task* task);

  size_t get_turn_count(size_t ms_offset = 0) const;
  bool is_accessible(MazeSquare *sq) const;
  bool is_safe(MazeSquare *sq) const;

  bool not_planned_to_plant = false;

private:
  size_t start_time = 0;
  std::deque<Task*> tasks {};
  Gladiator* gladiator = nullptr;

};
extern TaskScheduler * g_task_scheduler;

#endif // EXOLEGEND_TASK_HPP
