#include "bomb_task.hpp"
#include "movement_task.hpp"

//////////////////////////////////////////////////////////////////////

bool GetNearestBombTask::is_done() const {
  return true;
}

void GetNearestBombTask::update(Gladiator *gladiator) {
  gladiator->log("GetNearestBombTask");
  const auto current = gladiator->maze->getNearestSquare();
  MazeSquare* bomb_sq = nullptr;

  int dist = 2000000000;
  for(int i = 0; i < 12; ++i) {
    for (int j = 0; j < 12; ++j) {
      const auto sq = gladiator->maze->getSquare(i, j);
      if (sq != nullptr && sq->coin.value > 0
          && g_task_scheduler->is_safe(sq)) {
        const auto di = current->i - i;
        const auto dj = current->j - j;
        const auto d = di * di + dj * dj;
        if ((bomb_sq == nullptr || d < dist)) {
          bomb_sq = sq;
          dist = d;
        }
      }
    }
  }

  if (bomb_sq) {
    g_task_scheduler->add_task(new PathFindTask(bomb_sq));
  }
}

//////////////////////////////////////////////////////////////////////

bool LayDownBombTask::is_done() const {
  return bomb_planted; // the terrorist win
}

void LayDownBombTask::update(Gladiator *gladiator) {
  gladiator->log("LayDownBomb");
  gladiator->weapon->dropBombs(gladiator->weapon->getBombCount()); // drop all
  bomb_planted = true;
  g_task_scheduler->not_planned_to_plant = false;
}
