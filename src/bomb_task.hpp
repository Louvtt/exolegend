#ifndef EXOLEGEND_BOMB_TASK_HPP
#define EXOLEGEND_BOMB_TASK_HPP

#include "Task.hpp"

struct GetNearestBombTask : public Task {
  bool is_done() const override;
  void update(Gladiator *gladiator) override;
};

struct AvoidBombTask      : public Task { };

struct LayDownBombTask    : public Task {
  bool is_done() const override;
  void update(Gladiator *gladiator) override;

private:
  bool bomb_planted = false;
};

// struct GetBombTask : public Task { };

#endif // EXOLEGEND_BOMB_TASK_HPP
