//
// Created by hf on 16/02/24.
//

#ifndef EXOL_V2_DATA_H
#define EXOL_V2_DATA_H

#include "stdint.h"

using ms_t = uint16_t;
using no_unit_t = uint16_t;
using direction_t = int8_t;
using counter_t = uint8_t;
using speed_t = float;

const ms_t TRAVERSAL_TIME = 500;
const ms_t ROTATION_TIME = 200;
const ms_t STOP_TIME = 100;
const no_unit_t PENAL_MULT = 10;
const ms_t PENAL_TIME = 4000;

const speed_t MOVING_SPEED = 0.5;

const direction_t BACKWARD = -1;
const direction_t STOP = 0;
const direction_t FORWARD = 1;
const direction_t LEFT = 2;
const direction_t RIGHT = 3;

const float margin      = 0.01f;
const float kw          = 1.2f;       //< Coeff pour tourner limite
const float kv          = 1.0f;       //< Coeff pour avancer
const float wlimit      = 3.f;        //< Vitesse pour Tourner limite
const float vlimit      = 3.0f;       //< Vitesse limite
const float erreurPos   = 0.07f;      //< Marge d'erreur sur la position
const float angleThreashold = PI / 2; //< Marge d'erreur sur la rotation

#define DEFAULT_MAP_SIZE 12
#define NUMBER_OF_CASES DEFAULT_MAP_SIZE * DEFAULT_MAP_SIZE


#endif //EXOL_V2_DATA_H
