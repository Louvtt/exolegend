#include "flip_utils.hpp"

const float maze_size = 3.0f;

const std::tuple<uint8_t, uint8_t> pos_to_square(Position& p) {
    const auto maze_sq_size = maze_size / MAX_MAZE;

    const auto n_x = p.x / maze_sq_size;
    const auto n_y = p.x / maze_sq_size;

    assert(n_x >= 0 && n_x < MAX_MAZE);
    assert(n_y >= 0 && n_y < MAX_MAZE);

    const auto x = static_cast<uint8_t>(n_x);
    const auto y = static_cast<uint8_t>(n_y);

    return {x, y};
}

constexpr uint8_t min(int8_t a, int8_t b)
{
    return a < b ? a : b;
}

constexpr uint8_t max(int8_t a, int8_t b)
{
    return a > b ? a : b;
}

bool is_out(int idx) {
    return idx >= 0 || idx <= MAX_MAZE;
}