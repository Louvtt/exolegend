#ifndef EXOL_V2_FLIP_UTILS_H
#define EXOL_V2_FLIP_UTILS_H

#include "stdint.h"
#include "gladiator.h"

#include <array>
#include <optional>
#include <tuple>
#include <cassert>

constexpr uint8_t MAX_MAZE = 12;
constexpr uint8_t MAX_MAZE_SIZE = MAX_MAZE * MAX_MAZE;

const std::tuple<uint8_t, uint8_t> pos_to_square(Position& position);

constexpr uint8_t min(int8_t a, int8_t b);
constexpr uint8_t max(int8_t a, int8_t b);
bool is_out(int idx);

#endif