#include "gladiator.h"
#include "Task.hpp"

Gladiator *gladiator;

bool hasStarted = false;
void reset();
void setup()
{
    gladiator = new Gladiator();
    gladiator->game->onReset(&reset); // GFA 4.4.1

    gladiator->log("setup");
    g_task_scheduler->setup(gladiator);
    hasStarted = false;
}

void reset()
{
    g_task_scheduler->reset();
    hasStarted = false;
}

void loop()
{
    if (gladiator->game->isStarted())
    {
        if (!hasStarted) {
          hasStarted = true;
          g_task_scheduler->start();
        }
        g_task_scheduler->update();
    }
    else
    { }
    delay(100);
}