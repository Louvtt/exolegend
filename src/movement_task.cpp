#include "movement_task.hpp"
#include "bomb_task.hpp"

#include "pathfind.hpp"
#include "utils.hpp"

///////////////////////////////////////////////////////////////////////////////////

bool GetNearestOpenSquareTask::is_done() const {
  return true;
}
void GetNearestOpenSquareTask::update(Gladiator *gladiator) {
  gladiator->log("GetNearestOpenSquareTask");
  const auto current = gladiator->maze->getNearestSquare();
  std::vector<MazeSquare*> sqs { current->eastSquare, current->southSquare, current->westSquare, current->northSquare };

  while(1) {
    const int idx = rand() % 4;
    const auto sq = sqs.at(idx);
    if (sq != nullptr) {
      g_task_scheduler->add_task(new MoveToSquareTask(gladiator, sq));
      return;
    }
  }
}

///////////////////////////////////////////////////////////////////////////////////

MoveToSquareTask::MoveToSquareTask(Gladiator* gladiator, MazeSquare* target)
: gladiator(gladiator), target(target), reached(false) {
}

bool MoveToSquareTask::is_done() const {
  return reached;
}

void MoveToSquareTask::update(Gladiator *gladiator) {
  gladiator->log("MoveToSquare");
  reached = go_to(gladiator, target);
}

///////////////////////////////////////////////////////////////////////////////////

PathFindTask::PathFindTask(MazeSquare *target)
: target(target) {
}

bool PathFindTask::is_done() const { return true; }

void PathFindTask::update(Gladiator *gladiator) {
  gladiator->log("PathFindTask");
  auto res = pathfind_to(gladiator, gladiator->maze->getNearestSquare(), target);
  g_task_scheduler->add_task(new RouteTask(res));
}

///////////////////////////////////////////////////////////////////////////////////

RouteTask::RouteTask(const std::vector<MazeSquare*> route_vec)
    : aborted(false), route({}) {
  route.insert(route.begin(), route_vec.begin(), route_vec.end());
  // TODO(Eliot) : Travaille stp
}

bool RouteTask::is_done() const {
  return route.empty() || aborted;
}

void RouteTask::update(Gladiator *gladiator) {
  gladiator->log("Route Task (%zu nodes)", route.size());
  const auto next_target = route.front();
  if(!g_task_scheduler->is_accessible(next_target)) {
    aborted = true;
    g_task_scheduler->add_task(new GetNearestBombTask());
  } else {
    if (go_to(gladiator, next_target)) {
      route.pop_front();
    }
  }
}

///////////////////////////////////////////////////////////////////////////////////