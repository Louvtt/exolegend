#ifndef EXOLEGEND_MOVEMENT_TASK_HPP
#define EXOLEGEND_MOVEMENT_TASK_HPP

#include "Task.hpp"
#include <vector>

// DO NOT USE
struct GetNearestOpenSquareTask : public Task {
  bool is_done() const override;
  void update(Gladiator *gladiator) override;
};

struct PathFindTask : public Task {
  PathFindTask(MazeSquare *target);

  bool is_done() const override;
  void update(Gladiator *gladiator) override;
private:
  MazeSquare* target;
};

struct RouteTask : public Task {
  RouteTask(const std::vector<MazeSquare*> route);

  bool is_done() const override;
  void update(Gladiator *gladiator) override;
private:
  bool aborted;
  std::deque<MazeSquare*> route;
};

struct MoveToSquareTask : public Task {
  bool is_done() const override;
  void update(Gladiator *gladiator) override;

  MoveToSquareTask(Gladiator* gladiator, MazeSquare* target);
private:
  float kw     = 1.2;
  float kv     = 1.f;
  float wlimit = 3.f;
  float vlimit = 0.6;
  float erreurPos = 0.02;
  float angleThreashold = PI / 6;

  bool reached;
  Gladiator*  gladiator;
  MazeSquare* target;
};

// struct TurnTask : public Task { };

#endif // EXOLEGEND_MOVEMENT_TASK_HPP
