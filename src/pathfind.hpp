#ifndef EXOLEGEND_PATHFIND_HPP
#define EXOLEGEND_PATHFIND_HPP

#include "gladiator.h"

#include "data.h"

#include <unordered_map>
#include <vector>
#include <list>
#include <array>
#include <algorithm>
#include <cmath>
#undef abs

////////////////////////////////////////////////////////////////////////


struct ANode {
  MazeSquare * sq;
  int g, h;
};

struct Square {
  MazeSquare *square;
  bool direct_access;
};

#define IDX_MAP(i,j) ((i) * DEFAULT_MAP_SIZE + (j))

////////////////////////////////////////////////////////////////////////

static inline uint8_t dist(MazeSquare * current, MazeSquare * target)
{
  return std::abs(current->i - target->i) + std::abs(current->j - target->j) ;
}

static inline uint8_t heuristic(MazeSquare * current, MazeSquare * target, bool direct_access)
{
  return dist(current, target) + (direct_access ? 0 : 10);
}

size_t hash(MazeSquare* sq) {
  return ((sq->i << 8) + sq->j);
}

////////////////////////////////////////////////////////////////////////

std::vector<MazeSquare*>
pathfind_to(Gladiator *gladiator, MazeSquare *start_sq, MazeSquare *target)
{
  std::vector<ANode> scores;
  scores.reserve(NUMBER_OF_CASES);
  std::vector<MazeSquare*> open;
  open.reserve(NUMBER_OF_CASES);
  std::unordered_map<size_t, MazeSquare*> came_from{};

  //fill open & close nodes array
  for (int i = 0 ; i < DEFAULT_MAP_SIZE ; i++) {
    for (int j = 0; j < DEFAULT_MAP_SIZE; j++) {
      scores.push_back({gladiator->maze->getSquare(i, j), 255, 255});
    }
  }

  open.push_back(start_sq);
  auto start = &scores.at(IDX_MAP(start_sq->i, start_sq->j));
  start->g = 0; start->h = heuristic(start_sq, target, false);

  // A*
  while(!open.empty())
  {
    auto it = open.begin();
    auto cur = it;
    auto score = scores.at(IDX_MAP((*cur)->i, (*cur)->j));
    for(; it < open.end(); it++) {
      auto sq_score = scores.at(IDX_MAP((*it)->i, (*it)->j));
      if (sq_score.h < score.h) {
        cur = it;
        score = sq_score;
      }
    }

    auto current = *cur;
    if (current == target) {
      std::list<MazeSquare *> route { current };
      auto prev = came_from.find(hash(current));
      while (prev != came_from.end()) {
        current = prev->second;
        route.push_front(current);
        prev = came_from.find(hash(current));
      }
      std::vector<MazeSquare*> route_vec(route.begin(), route.end());
      return route_vec;
    }
    open.erase(cur);

    // for each neighbors
    std::array<Square,4> neighbors {
        Square{ gladiator->maze->getSquare(current->i+1, current->j  ), current->eastSquare  != nullptr},
        Square{ gladiator->maze->getSquare(current->i  , current->j-1), current->southSquare != nullptr},
        Square{ gladiator->maze->getSquare(current->i-1, current->j  ), current->westSquare  != nullptr},
        Square{ gladiator->maze->getSquare(current->i  , current->j+1), current->northSquare != nullptr}
    };
    for (auto neighbor : neighbors)
    {
      if (neighbor.square == nullptr) continue;

      auto idx = IDX_MAP(neighbor.square->i, neighbor.square->j);
      auto tscore = score.g + dist(current, neighbor.square);

      if (tscore  < scores.at(idx).g) {
        scores.at(idx).g = tscore;
        scores.at(idx).h = tscore + heuristic(current, neighbor.square, neighbor.direct_access);
        came_from.insert({ hash(neighbor.square), current });
        if (std::find_if(open.begin(), open.end(), [&](MazeSquare* b){ return b->i == neighbor.square->i && b->j == neighbor.square->j; }) == open.end()) {
          open.push_back(neighbor.square);
        }
      }
    }
  }
  return {};
}

#endif // EXOLEGEND_PATHFIND_HPP
