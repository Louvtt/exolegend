#include "utils.hpp"


#include <vector>
#include <cmath>
#undef abs

static inline double reductionAngle(double x)
{
  x = fmod(x + PI, 2 * PI);
  if (x < 0)
    x += 2 * PI;
  return x - PI;
}

bool go_to(Gladiator *gladiator, MazeSquare *target)
{
  const auto current = gladiator->robot->getData().position;
  const auto square_size = gladiator->maze->getSquareSize();
  const auto targetPos = Position{ ((float)target->i + .5f) * square_size, ((float)target->j + .5f) * square_size };
  double consvl, consvr;
  const double dx = targetPos.x - current.x;
  const double dy = targetPos.y - current.y;
  const double d = sqrt(dx * dx + dy * dy);
  bool reached = false;

  const auto current_sq = gladiator->maze->getNearestSquare();
  const float cx = current.x - (current_sq->i * square_size);
  const float cy = current.x - (current_sq->j * square_size);
  const bool in_wall = false; // (cx < margin || (1.f-margin) < cx) || (cy < margin || (1.f-margin) < cy);

  // if not reached or in wall (overshoot)
  if (d > erreurPos || in_wall) {
    double rho = atan2(dy, dx);
    double angleErr = reductionAngle(rho - current.a);
    double consw = kw * angleErr;

    double consv = kv * d * cos(reductionAngle(rho - current.a));
    consw = abs(consw) > wlimit ? (consw > 0 ? 1 : -1) * wlimit : consw;
    consv = abs(consv) > vlimit ? (consv > 0 ? 1 : -1) * vlimit : consv;

    if (std::abs(angleErr) > angleThreashold) {
      consvl = - gladiator->robot->getRobotRadius() * consw; // GFA 3.6.2
      consvr = + gladiator->robot->getRobotRadius() * consw; // GFA 3.6.2
    } else {
      consvl = consv - gladiator->robot->getRobotRadius() * consw; // GFA 3.6.2
      consvr = consv + gladiator->robot->getRobotRadius() * consw; // GFA 3.6.2
    }
  } else {
    consvr = 0;
    consvl = 0;
    reached = true;
  }

  gladiator->control->setWheelSpeed(WheelAxis::RIGHT, consvr, false); // GFA 3.2.1
  gladiator->control->setWheelSpeed(WheelAxis::LEFT, consvl, false);  // GFA 3.2.1
  return reached;
}

/////////////////////////////////////////////////////////////////////////////////////////

MazeSquare * getNeighbor(Gladiator * gladiator, MazeSquare * sq, direction dir)
{
  int8_t sign = dir == EAST || dir == NORTH ? 1 : -1;
  bool change_i = dir == EAST || dir == WEST;

  if ( sq->westSquare != nullptr )
    return sq->westSquare;
  else if ( (sign == -1 && change_i && sq->i <= DEFAULT_MAP_SIZE - 1) ||
           (sign == 1 && change_i && sq->i >= DEFAULT_MAP_SIZE - 1)  ||
           (sign == -1 && !change_i && sq->j <= 0)                   ||
           (sign == 1 && !change_i && sq->j <= 0) )
    return nullptr;
  else
  {
    MazeSquare * toLookup = nullptr;
    for (uint8_t inc = 2 ; toLookup != nullptr && inc < 7; inc++ )
      toLookup = gladiator->maze->getSquare(sq->i + (change_i ? sign * inc : 0), sq->j + (!change_i ? sign * inc : 0));

    return toLookup;
  }
}

std::array<MazeSquare *,4> getNeighborSquares(Gladiator * gladiator, MazeSquare * sq)
{
  return
      {
          getNeighbor(gladiator, sq, EAST),
          getNeighbor(gladiator, sq, SOUTH),
          getNeighbor(gladiator, sq, WEST),
          getNeighbor(gladiator, sq, NORTH)
      };
}
