#ifndef EXOLEGEND_UTILS_HPP
#define EXOLEGEND_UTILS_HPP

#include "gladiator.h"
#include "data.h"

#include <array>

bool go_to(Gladiator *gladiator, MazeSquare *target);

enum direction {
  EAST,
  SOUTH,
  WEST,
  NORTH
};

MazeSquare * getNeighbor(Gladiator * gladiator, MazeSquare * sq, direction dir);

std::array<MazeSquare *,4> getNeighborSquares(Gladiator * gladiator, MazeSquare * sq);

#endif // EXOLEGEND_UTILS_HPP
